FROM nginx:1.21.1
LABEL maintainer="EAZETRAINNING"
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl && \
    apt-get install -y git
RUN rm -Rf /usr/share/nginx/html/*
RUN git clone https://github.com/diranetafen/static-website-example.git /usr/share/nginx/html
COPY ngnix.conf /etc/ngnix/conf.d/default.conf
#CMD ngnix.conf /etc/nginx/conf.d/default.conf
#CMD nginx -g 'daemon off;'
CMD ["nginx", "-g", "daemon off;"]
